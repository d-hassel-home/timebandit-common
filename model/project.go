package model

import (
	"fmt"
	"github.com/google/uuid"
	"time"
)

// Project is a unique object with a short name and a description aka long name
type Project struct {
	UUID      uuid.UUID "json:uuid"
	ShortName string    "json:short-name"
	LongName  string    "json:long-name"
	Created   time.Time "json:created"
}

func (p *Project) String() string {
	return fmt.Sprintf("project (%s/%s) created on %s \n => %s", p.ShortName, p.UUID, p.Created, p.LongName)
}
